runtime! syntax/css.vim

match CSMAMacro /<[^>]*>/
syntax match CSMAComment /^\/\/.*$/

hi CSMAMacro cterm=NONE ctermfg=White
hi CSMAComment ctermfg=244
