[Test 1: Simple line substitution]

div {
  color: black;
}
<1A>
div {
  color: black;
}

<1A> {
  margin: 0;
}

div {
  color: black;
}
<1A>
div {
  color: black;
}

[Test 2: Simple line substitution with CSS rules inside]

div {
  color: black;
}
<2A>
div {
  color: black;
}

<2A> {
  a {
    margin: 0;
  }
  b {
    padding: 1;
  }
}

div {
  color: black;
}
<2A>
div {
  color: black;
}

[Test 3: Inner substitution]

div {
  color: <3A>;
}

<3A> {
  white
}

div {
  color: <3A>;
}

[Test 5: Same definition names append]

<5A> {
  color: black;
}
<5A>
<5A> {
  margin: 0;
}

[Test 6: Different "{" placement on definition]

<6A> {
  a
}

<6B>
{
  b
}

<6C>


{
  i
}

<6A>
<6B>
<6C>

[Test 7: Different "}" placement on definition]

<7A> {
  1
}
<7B> {
  2 }
 
<7C> { 3
}

<7D> { 4 }

<7E>

{
  1
}
<7F>

{
  2 }
 
<7G>

{ 3
}

<7H>

{ 4 }

<7A>
<7B>
<7C>
<7D>
<7E>
<7F>
<7G>
<7H>

[Test 8: Multiple substitutions on same line]

<8A><8B><8C>
a<8A><8B><8C>
<8A>a<8B><8C>
<8A><8B>a<8C>
<8A><8B><8C>a
a<8A><8B>a<8C>a
a<8A>a<8B><8C>a

<8A> {
  1
}
<8B> {
  2
}
<8C> {
  3
}

[Test 9: Substitution before definition is not allowed]

<9B> <9A> {
  color: black;
}

<9B> <9B> {
  color: white;
}

[Test 10: Inner substitutions]

<10A> {
  a
}

<10B> {
  div {
    color: white;
  }
  <10A>
  div {
    color: white;
  }
}

<10C> {
  div {
    color: white;
  }
  span {
    padding: <10A>;
  }
  div {
    color: white;
  }
}

<10B>
<10C>

<10A> {
  b
}

[Test 11: One level recursive inner substitution]

<11A>
<11A> {
  color: white;
  <11A>
}

[Test 12: Inner definition is placed in parent]

<12A>
<12A> {
  div {
    color: white;
  }
  <12B> {
    div {
      color: black;
    }
  }
  div {
    color: white;
  }
}

[Test 13: Accessing inner definition]

<13A/13B>
<13A> {
  div {
    color: white;
  }
  <13B> {
    div {
      color: black;
    }
  }
  div {
    color: white;
  }
}

[Test 14: Inner definition appending and outside definition]

<14A/B>
<14A/B> {
  height: 8;
}
<14A> {
  color: white;
  <B> { margin: 0; }
}
<14A> {
  <B> {
    width: 3;
  }
}
<14A/B> {
  padding: 10;
}

[Test 15: Inner definition placement in parent on append]

<15C> {
  width: 100%;
}

<15A> {
  a
  <15C>
  <B> {}
  b
  <15C>
}

<15A> {
  <B> {
    color: white;
  }
}

<15A>

[Test 16: Same line closing braces on parent and child]

<16A> {
  <B> {
    color: white;
}}

<16A>
a
<16A/B>
